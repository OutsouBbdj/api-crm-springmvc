package fr.eftl.dev.api.springboot.crm.repository;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.eftl.dev.api.springboot.crm.model.User;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class UserRepositoryImplTest {

	@Autowired
	private UserRepository userRepository;

	@Test
	@Order(1)
	void whenCallingFindMethodByUsername_thenReturnUser() {
		User user = userRepository.findByUsername("toto");

		Assertions.assertEquals("toto@titi.com", user.getMail(), "Toto est perdu ?");
	}

	@Test
	@Order(2)
	void whenCallingFindMethodByUsernameEtMotDePass_thenReturnUser() {
		User user = userRepository.findByUsernameAndPassword("toto", "$2y$10$KhQBkrOov7kZu9Rj7gYJI.5k8AYIo8B0iIhfY3PItbKCLcCYomGf.");
		Assertions.assertEquals("toto@titi.com", user.getMail(), "Toto est perdu ?");

	}

	@Test
	@Order(3)
	void testCreate() {
		User newUser = new User();
		newUser.setUsername("titi");
		newUser.setPassword("1234");
		newUser.setMail("titi@titi.com");
		newUser.setGrants("ADMIN");

		userRepository.save(newUser);

		User user = userRepository.findByUsername("titi");
		Assertions.assertEquals("titi", user.getUsername());
	}

	@Test
	@Order(4)
	void testUpdate() {
		User user = userRepository.findByUsername("toto");
		user.setGrants("ADMIN");

		userRepository.save(user);

		User updatedUser = userRepository.findByUsername("toto");
		Assertions.assertEquals("ADMIN", updatedUser.getGrants());
	}

	@Test
	@Order(5)
	void testDelete() {
		Optional<User> user = userRepository.findById(1);
		if (user.isEmpty()) {
			fail();
		}
		userRepository.delete(user.get());

		Optional<User> deleteUser = userRepository.findById(1);
		Assertions.assertTrue(deleteUser.isEmpty(), "Deleted user must be null");

	}
}
