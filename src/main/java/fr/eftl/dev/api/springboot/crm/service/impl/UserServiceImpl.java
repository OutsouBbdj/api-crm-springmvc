package fr.eftl.dev.api.springboot.crm.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.app.exception.UserExistException;
import fr.eftl.dev.api.springboot.crm.model.User;
import fr.eftl.dev.api.springboot.crm.repository.UserRepository;
import fr.eftl.dev.api.springboot.crm.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	public static final String ERROR_MSG_NO_USER = "Utilisateur inexistant!";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User getUserById(Integer id) {
		return userRepository.findById(id).orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_USER));
	}

	@Override
	public User createUser(User user) {
		// validate is username exist
		if (null != userRepository.findByUsername(user.getUsername())) {
			throw new UserExistException();
		}
		return userRepository.save(user);
	}

	@Override
	public User updateUser(User user, boolean changePassword) {
		// validate is username alraedy exist
		User userNameExist = userRepository.findByUsername(user.getUsername());
		// verify is the exist username belongs to the same user, if yes, should not
		// throw error
		if (userNameExist != null && !userNameExist.getId().equals(user.getId())) {
			throw new UserExistException();
		}

		logger.debug("attempting to update user {}...", user.getId());
		User utilisateurExistant = userRepository.findById(user.getId())
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_USER));
		utilisateurExistant.setId(user.getId());
		utilisateurExistant.setUsername(user.getUsername());
		utilisateurExistant.setMail(user.getMail());
		utilisateurExistant.setGrants(user.getGrants());
		if (changePassword) {
			utilisateurExistant.setPassword(user.getPassword());
		}
		return userRepository.save(utilisateurExistant);
	}

	@Override
	public void deleteUser(Integer id) {
		User userToDelete = userRepository.findById(id)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_USER));
		userRepository.delete(userToDelete);
	}

	@Override
	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
		User user = userRepository.findByUsername(username);

		if (user != null) {
			boolean passwordOk = bCryptPasswordEncoder.matches(password, user.getPassword());
			if (!passwordOk) {
				return null;
			}
		}
		return user;
	}

}
