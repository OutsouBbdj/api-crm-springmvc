package fr.eftl.dev.api.springboot.crm.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.eftl.dev.api.springboot.crm.model.Order;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class ProductDto {

	private Integer id;
	
	@NotNull
	@Size(min = 2, max = 100)
	private String productname;
	
	@Size(max = 255)
	private String description;
	
	@NotNull
	private Boolean availablity;
	
	@Nullable
	private List<Order> orders;
	
	
	
	
	

	public ProductDto(Integer id, String productname, String description,
			 Boolean availablity) {
		super();
		this.id = id;
		this.productname = productname;
		this.description = description;
		this.availablity = availablity;
	}

	public ProductDto() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getAvailablity() {
		return availablity;
	}

	public void setAvailablity(Boolean availablity) {
		this.availablity = availablity;
	}

	public List<Order> getOrders() {
		return orders;
	}
	
	public Boolean isAvailablity() {
		return availablity;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
	
}
