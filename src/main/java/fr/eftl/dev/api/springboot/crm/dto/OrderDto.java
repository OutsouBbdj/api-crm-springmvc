package fr.eftl.dev.api.springboot.crm.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDto {

	private Integer id;
	
	/** average daily rate excluding tax adr_et */
	@NotNull
	private Double adrEt;
	@NotNull
	private Double numberOfDays;
	@NotNull
	private Double tva;
	@NotNull
	@Size(min = 1, max = 20)
	private String status;
	@NotNull
	@Size(min = 1, max = 100)
	private String type;
	@Size(max = 255)
	private String notes;
	
	private	ProductDto productDto;


	private CustomerDto customerDto;

	public OrderDto(Integer id, String label, Double adrEt, String status, String type, String notes,
			Double numberOfDays, CustomerDto customerDto, Double tva, ProductDto productDto) {
		super();
		this.id = id;
		this.adrEt = adrEt;
		this.status = status;
		this.type = type;
		this.notes = notes;
		this.numberOfDays = numberOfDays;
		this.customerDto = customerDto;
		this.productDto = productDto;
		this.tva = tva;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Double getAdrEt() {
		return adrEt;
	}

	public void setAdrEt(Double adrEt) {
		this.adrEt = adrEt;
	}

	public Double getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(Double numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public Double getTva() {
		return tva;
	}

	public void setTva(Double tva) {
		this.tva = tva;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public CustomerDto getCustomerDto() {
		return customerDto;
	}

	public void setCustomerDto(CustomerDto customerDto) {
		this.customerDto = customerDto;
	}
	

	public ProductDto getProductDto() {
		return productDto;
	}

	public void setProductDto(ProductDto productDto) {
		this.productDto = productDto;
	}

	public OrderDto() {
	}

}
