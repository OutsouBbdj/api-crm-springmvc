package fr.eftl.dev.api.springboot.crm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
@NamedQuery(name = "Product.findByAvailablity", 
query = "SELECT p FROM Product p WHERE p.availablity=:availablity")
public class Product implements Serializable {
	
	private static final long serialVersionUID = -5179286446559866248L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length = 255)
	private String productname;
	
	@Column(columnDefinition = "TEXT")
	private String description;
	
	private Boolean availablity;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	private List<Order> orders;

	public Product(Integer id, String productname, String description,
			Boolean availablity) {
		super();
		this.id = id;
		this.productname = productname;
		this.description = description;
		this.availablity = availablity;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", Productname=" + productname +
				", description=" + description + ", availablity="
				+ availablity +  "]";
	}

	public Product() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Boolean isAvailablity() {
		return availablity;
	}

	public Boolean getAvailablity() {
		return availablity;
	}

	public void setAvailablity(Boolean availablity) {
		this.availablity = availablity;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	

}
