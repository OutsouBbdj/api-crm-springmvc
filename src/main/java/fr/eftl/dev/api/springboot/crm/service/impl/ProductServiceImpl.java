/**
 * 
 */
package fr.eftl.dev.api.springboot.crm.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eftl.dev.api.springboot.crm.app.exception.CustomerWithOrderDeleteException;
import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.model.Product;
import fr.eftl.dev.api.springboot.crm.repository.CustomerRepository;
import fr.eftl.dev.api.springboot.crm.repository.OrderRepository;
import fr.eftl.dev.api.springboot.crm.repository.ProductRepository;
import fr.eftl.dev.api.springboot.crm.service.ProductService;

/**
 * @author pc
 *
 */
@Service
public class ProductServiceImpl implements ProductService {
	
	Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

	public static final String ERROR_MSG_NO_PRODUCT = "Aucun produit trouvé!";

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private OrderRepository orderRepository;

	
	@Override
	public List<Product> getAllProducts() {
		
		return productRepository.findAll();
	}

	@Override
	public List<Product> getProductsByAvailablity(Boolean availablity) {
		return productRepository.findByAvailablity(availablity);
	}

	@Override
	public Product updateProduct(Product product) {

		logger.debug("attempting to update product {}...", product.getId());
		Product produitExistant = productRepository.findById(product.getId())
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_PRODUCT));
		produitExistant.setProductname(product.getProductname());
		produitExistant.setDescription(product.getDescription());
		produitExistant.setAvailablity(product.isAvailablity());
		return productRepository.save(produitExistant);
	}

	@Override
	public Product createProduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public void deleteProduct(Integer productId) {
		Product product = productRepository.findById(productId)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_PRODUCT));
		// validate if customer has order before delete
		List<Order> prodOrderExist = orderRepository.findByProduct(product);
		if (null != prodOrderExist && !prodOrderExist.isEmpty()) {
			throw new CustomerWithOrderDeleteException();
		}
		productRepository.delete(product);

	}

	@Override
	public Product getProductById(Integer productId) {
		return productRepository.findById(productId)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_PRODUCT));
	}

}
