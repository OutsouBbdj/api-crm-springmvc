package fr.eftl.dev.api.springboot.crm.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnknownResourceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 780405970648543635L;

	public UnknownResourceException() {
		super("UnknownResourceException");
	}

	public UnknownResourceException(String message) {
		super(message);
	}

	public UnknownResourceException(Throwable cause) {
		super(cause);
	}
}
