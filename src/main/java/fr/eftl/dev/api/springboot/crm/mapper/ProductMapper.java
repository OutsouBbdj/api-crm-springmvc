/**
 * 
 */
package fr.eftl.dev.api.springboot.crm.mapper;

import org.springframework.stereotype.Component;

import fr.eftl.dev.api.springboot.crm.dto.ProductDto;
import fr.eftl.dev.api.springboot.crm.model.Product;

/**
 * @author pc
 *
 */
@Component
public class ProductMapper {
	
	public ProductDto mapProductToProductDto(Product product) {
		ProductDto productDto = new ProductDto();
		productDto.setId(product.getId());
		productDto.setProductname(product.getProductname());
		productDto.setDescription(product.getDescription());
		productDto.setAvailablity(product.isAvailablity());
		return productDto;
	}

	public Product mapProductDtoToProduct(ProductDto productDto) {
		Product product = new Product();
		product.setId(productDto.getId());
		product.setProductname(productDto.getProductname());
		product.setDescription(productDto.getDescription());
		product.setAvailablity(productDto.isAvailablity());
		return product;
	}

}
