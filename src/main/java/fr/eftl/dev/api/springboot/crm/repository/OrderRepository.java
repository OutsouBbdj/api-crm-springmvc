package fr.eftl.dev.api.springboot.crm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.model.Product;

public interface OrderRepository extends JpaRepository<Order, Integer> {

	Optional<List<Order>> findByTypeAndStatus(String type, String status);

	List<Order> findByCustomer(Customer customer);
	
	List<Order> findByProduct(Product product);

	Optional<List<Order>> findByStatus(String status);

	List<Order> findByType(String type);

}
