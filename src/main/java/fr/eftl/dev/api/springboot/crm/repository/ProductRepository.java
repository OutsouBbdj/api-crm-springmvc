/**
 * 
 */
package fr.eftl.dev.api.springboot.crm.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import fr.eftl.dev.api.springboot.crm.model.Product;

/**
 * @author pc
 *
 */

public interface ProductRepository extends JpaRepository<Product, Integer> {

	Product findByProductname(String productname);
	
	List<Product> findByAvailablity(@Param("availablity") Boolean availablity);
	
	Page<Product> findPageByAvailablity(Boolean availablity, Pageable pageable);

}
