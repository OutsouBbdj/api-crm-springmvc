package fr.eftl.dev.api.springboot.crm.service;

import java.util.List;

import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.model.Product;

public interface OrderService {

	/**
	 * Get all orders
	 * 
	 * @return a list of orders
	 */
	List<Order> getAllOrders();

	/**
	 * Get an order by id
	 * 
	 * @param id the id
	 * @return the order
	 */
	Order getOrderById(Integer id);

	/**
	 * Create an order
	 * 
	 * @param order the order to create
	 * @return the order
	 */
	Order createOrder(Order order);

	/**
	 * Update an order
	 * 
	 * @param order the order to update
	 * @return the order
	 * @throws Exception
	 */
	Order updateOrder(Order order) throws Exception;

	/**
	 * Delete an order
	 * 
	 * @param id the id of the order to delete
	 */
	void deleteOrder(Integer id);

	/**
	 * Patch the status of an order
	 * 
	 * @param orderId the id
	 * @param status  the status to patch
	 * @throws Exception
	 */
	void patchOrderStatus(Integer orderId, String status) throws Exception;

	List<Order> getOrderByTypeAndStatus(String type, String status) throws Exception;

	List<Order> getOrderByStatus(String status) throws Exception;

	List<Order> getOrderByCustomer(Customer customer) throws Exception;

	List<Order> getOrderByProduct(Product product) throws Exception;

}
