/**
 * 
 */
package fr.eftl.dev.api.springboot.crm.service;

import java.util.List;


import fr.eftl.dev.api.springboot.crm.model.Product;

/**
 * @author pc
 *
 */
public interface ProductService {

	List<Product> getAllProducts();

	List<Product> getProductsByAvailablity(Boolean availablity);

	Product updateProduct(Product product);

	Product createProduct(Product product);

	void deleteProduct(Integer productId);

	Product getProductById(Integer productId);

}
