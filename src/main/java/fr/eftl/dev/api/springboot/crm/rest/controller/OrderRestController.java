package fr.eftl.dev.api.springboot.crm.rest.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.dev.api.springboot.crm.dto.OrderDto;
import fr.eftl.dev.api.springboot.crm.mapper.OrderMapper;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.model.Product;
import fr.eftl.dev.api.springboot.crm.service.CustomerService;
import fr.eftl.dev.api.springboot.crm.service.OrderService;
import fr.eftl.dev.api.springboot.crm.service.ProductService;

@Validated
@RestController
@RequestMapping("/orders")
@CrossOrigin(value = { "*" }, allowedHeaders = { "*" })
public class OrderRestController {

	@Autowired
	private OrderService orderService;
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ProductService productService;

	@GetMapping()
	public ResponseEntity<List<OrderDto>> getOrders(
			@Size(min = 1, max = 100) @RequestParam(value = "type", required = false) String type,
			@Size(min = 1, max = 20) @RequestParam(value = "status", required = false) String status) {
		List<Order> ordersList = new ArrayList<>();
		List<OrderDto> ordersListDto = new ArrayList<>();
		OrderMapper orderMapper = new OrderMapper();

		if (type != null && status != null) {
			try {
				ordersList = orderService.getOrderByTypeAndStatus(type, status);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} else {
			if (status != null) {
				try {
					ordersList = orderService.getOrderByStatus(status);
				} catch (Exception e) {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			} else
				try {
					ordersList = orderService.getAllOrders();
				} catch (Exception e) {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}

			if (null == ordersList) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
		for (Order order : ordersList) {
			OrderDto orderDto = orderMapper.mapOrderToOrderDto(order);
			ordersListDto.add(orderDto);
		}
		return ResponseEntity.ok().body(ordersListDto);
	}

	@GetMapping("/{id}")
	public ResponseEntity<OrderDto> getOrderById(@NotNull @PathVariable Integer id) {
		Order order = new Order();
		try {
			order = orderService.getOrderById(id);
			if (null == order) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		OrderMapper mapper = new OrderMapper();
		return ResponseEntity.ok().body(mapper.mapOrderToOrderDto(order));
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity<List<OrderDto>> getOrdersByCustomer(@NotNull @PathVariable Integer id) {
		Customer customer = new Customer();
		customer.setId(id);
		List<Order> ordersList = new ArrayList<>();
		List<OrderDto> ordersListDto = new ArrayList<>();
		try {
			ordersList = orderService.getOrderByCustomer(customer);
			if (ordersList == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		OrderMapper orderMapper = new OrderMapper();
		for (Order order : ordersList) {
			OrderDto orderDto = orderMapper.mapOrderToOrderDto(order);
			ordersListDto.add(orderDto);
		}
		return ResponseEntity.ok().body(ordersListDto);

	}
	
	@GetMapping("/products/{id}")
	public ResponseEntity<List<OrderDto>> getOrdersByProduct(@NotNull @PathVariable Integer id) {
		Product product = new Product();
		product.setId(id);
		List<Order> ordersList = new ArrayList<>();
		List<OrderDto> ordersListDto = new ArrayList<>();
		try {
			ordersList = orderService.getOrderByProduct(product);
			if (ordersList == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		OrderMapper orderMapper = new OrderMapper();
		for (Order order : ordersList) {
			OrderDto orderDto = orderMapper.mapOrderToOrderDto(order);
			ordersListDto.add(orderDto);
		}
		return ResponseEntity.ok().body(ordersListDto);

	}

	@PostMapping()
	public ResponseEntity<OrderDto> createOrder(@Valid @RequestBody OrderDto orderDto) {
		OrderMapper mapper = new OrderMapper();
		Order order = mapper.mapOrderDtoToOrder(orderDto);
		// create new customer
		if (null == order.getCustomer().getId()) {
			Customer newCustomer = customerService.createCustomer(order.getCustomer());
			order.setCustomer(newCustomer);
		}
		
		if (null == order.getProduct().getId()) {
			Product newProduct = productService.createProduct(order.getProduct());
			order.setProduct(newProduct);
		}
		
		Order newOrder = orderService.createOrder(order);
		return ResponseEntity.created(URI.create("/orders/" + newOrder.getId()))
				.body(mapper.mapOrderToOrderDto(newOrder));
	}


	@PutMapping("/{id}")
	public ResponseEntity<Void> updateCustomer(@Valid @RequestBody OrderDto orderDto,
			@NotNull @PathVariable Integer id) {
		if (null == orderService.getOrderById(id)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		try {
			OrderMapper orderMapper = new OrderMapper();
			Order orderToBeUpdated = orderMapper.mapOrderDtoToOrder(orderDto);
			orderToBeUpdated.setId(id);
			orderService.updateOrder(orderToBeUpdated);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<Void> patchOrderStatus(@NotNull @PathVariable Integer id,
			@NotNull @Size(min = 1, max = 20) @RequestParam("status") String status) {
		if (null == orderService.getOrderById(id)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		try {
			orderService.patchOrderStatus(id, status);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteOrder(@NotNull @PathVariable(value = "id") Integer id) {
		if (null == orderService.getOrderById(id)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			orderService.deleteOrder(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
