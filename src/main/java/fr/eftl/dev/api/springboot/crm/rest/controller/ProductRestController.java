/**
 * 
 */
package fr.eftl.dev.api.springboot.crm.rest.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.dev.api.springboot.crm.dto.ProductDto;
import fr.eftl.dev.api.springboot.crm.mapper.ProductMapper;
import fr.eftl.dev.api.springboot.crm.model.Product;
import fr.eftl.dev.api.springboot.crm.service.ProductService;

/**
 * @author pc
 *
 */
@Validated
@RestController
@RequestMapping("/products")
@CrossOrigin(value = { "*" }, allowedHeaders = { "*" })
public class ProductRestController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductMapper productMapper;
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{id}")
	public ResponseEntity<ProductDto> getProductById(@NotNull @PathVariable(value = "id") Integer id){
		Product product = productService.getProductById(id);
		
		if (product == null) {
			return (ResponseEntity<ProductDto>) ResponseEntity.notFound();
		}

		return ResponseEntity.ok().body(productMapper.mapProductToProductDto(product));
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping()
	public ResponseEntity<List<ProductDto>> getAllProducts() {
		List<Product> products;
		products = productService.getAllProducts();

		if (products.isEmpty()) {
			return (ResponseEntity<List<ProductDto>>) ResponseEntity.notFound();
		}

		List<ProductDto> dtoProducts = new ArrayList<>();
		for (Product product : products) {
			dtoProducts.add(productMapper.mapProductToProductDto(product));
		}

		return ResponseEntity.ok().body(dtoProducts);
	}
		
	@GetMapping("/IsAvailablity")
	public ResponseEntity<List<ProductDto>> getProductsByAvailablity(@RequestParam boolean availablity) {
		List<Product> productsAvailablity = productService.getProductsByAvailablity(availablity);

		List<ProductDto> dtoProducts = new ArrayList<>();
		for (Product product : productsAvailablity) {
			dtoProducts.add(productMapper.mapProductToProductDto(product));
		}
		return ResponseEntity.ok().body(dtoProducts);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody 
			final ProductDto productDto,
			@NotNull @PathVariable Integer id) {
		Product productToBeUpdated = productMapper.mapProductDtoToProduct(productDto);
		productToBeUpdated.setId(id);
		Product updatedProduct = productService.updateProduct(productToBeUpdated);
		return ResponseEntity.ok().body(productMapper.mapProductToProductDto(updatedProduct));
	}
	
	@PostMapping()
	public ResponseEntity<ProductDto> createProduct(@Valid @RequestBody
			final ProductDto productDto) {
		Product product = productMapper.mapProductDtoToProduct(productDto);
		Product newProduct = productService.createProduct(product);

		return ResponseEntity.created(URI.create("/products/" + newProduct.getId()))
				.body(productMapper.mapProductToProductDto(newProduct));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteProduct(@NotNull @PathVariable(value = "id") Integer id) {
		productService.deleteProduct(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
