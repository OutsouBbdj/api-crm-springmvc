package fr.eftl.dev.api.springboot.crm.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserExistException extends RuntimeException {

	private static final long serialVersionUID = 6355843250498930270L;

	public UserExistException() {
		super("Ce nom d'utilisateur existe déjà ! Changez de nom.");
	}

	public UserExistException(String message) {
		super(message);
	}

	public UserExistException(Throwable cause) {
		super(cause);
	}
}
